from django.shortcuts import render
import requests, datetime, calendar
from . import api_keys

# The keys we are hoping to find, ideally.
# There are more options, but sticking with the basics here; not sure if things like dew point and pressure
# count as "useful information"
# DarkSky API lists many as 'optional' so must check for presence of keys
# See https://darksky.net/dev/docs for explanations of data points
desired_historical_keys = ['precipProbability', 'temperatureHigh', 'temperatureLow']
hourly_desired_keys = ['icon', 'cloudCover', 'humidity', 'precipProbability', 'temperature', 'uvIndex']
daily_desired_keys = ['icon', 'cloudCover', 'humidity', 'precipProbability', 'precipIntensity',
                      'precipType', 'temperature', 'uvIndex', 'moonPhase', 'sunriseTime',
                      'sunsetTime', 'temperatureHigh', 'temperatureHighTime', 'temperatureLow',
                      'temperatureLowTime', 'uvIndex']
# implement alerts?
alerts_desired_keys = ['expires', 'severity', 'title', 'uri']

error = False;

def query_exists(street_name, city_name, num_queries, session):
    return_value = False;
    # for every previous query, compare against the current query's street/city name to prevent repeat queries
    for i in range(0, num_queries):
        if session['query'+str(i)]['street'] == street_name and session['query'+str(i)]['city'] == city_name:
            return_value = True;
    # if return true, this query has already been made and should not be stored as a new session value
    return return_value


def index_view(request):
    global error;
    context={}

    if error:
        context['error'] = 'True'
        error = False

    # if count not found, initialize count of how many queries have been made to 0
    if 'number_of_queries' not in request.session:
        request.session['number_of_queries'] = 0
    # if at at least one query has been made, we can let index know there are prior queries to display
    elif request.session['number_of_queries'] > 0:
        num_queries = request.session['number_of_queries']
        context["number_of_queries"] = num_queries
        # list of dicts, e.g. queries_list[0] = {'street': '1111 BlueWolf Avenue', ... }
        queries_list = [None] * request.session['number_of_queries']
        for i in range(0,num_queries):
            # e.g. queries_list[0] = request.session['query0']
            queries_list[i] = request.session['query'+str(i)]
        context['queries'] = queries_list
    return render(request, 'app_skycast/index.html', context)


def current_view(request):
    global error
    # retrieve form input values for the address
    street = request.POST['input1']
    city = request.POST['input2']
    state = request.POST['input3']

    # retrieve lat/lng with Google geocoding API
    location = get_lat_lng(street, city, state)

    # if lat/lng lookup didn't work, set global 'error' var to true and call index view
    if location[0] == None:
        error=True
        return index_view(request)

    query_number = request.session['number_of_queries']
    if query_exists(street, city, query_number, request.session):
        pass
    else:
        query_info = {'street': street, 'city': city, 'state': state}
        # starts at request.session['query0'], then 'query1', 'query2', ... etc.
        request.session['query'+str(query_number)] = query_info
        # incrementing number_of_queries
        request.session['number_of_queries'] += 1

    # use the lat/lng coordinates to query Dark Sky
    forecast_json = get_forecast(location[0], location[1], request.session)
    # picking out the section on current weather
    currently_json = forecast_json['currently']
    current_weather_info = {}

    # The keys we are hoping to find, ideally.
    # There are more options, but sticking with the basics here; not sure if things like dew point and pressure
    # count as "useful information"
    # DarkSky API lists many as 'optional' so must check for presence of each key
    desired_keys = ['cloudCover', 'humidity', 'icon', 'precipType', 'precipProbability',
                    'temperature', 'uvIndex']

    # For every desired key found in the 'currently' forecast response, add that key:value pair to
    # current_weather_info dict
    for key in desired_keys:
        current_weather_info[key] = '-'
        if key in currently_json:
            current_weather_info[key] = currently_json[key]

    # converting proportions to percents (e.g. 0.08 to 8)
    if 'cloudCover' in current_weather_info:
        current_weather_info['cloudCover'] *= 100

    if 'precipProbability' in current_weather_info:
        current_weather_info['precipProbability'] *= 100

    if 'humidity' in current_weather_info:
        current_weather_info['humidity'] *= 100

    # Converting epoch/POSIX/Unix timestamp to datetime.datetime object containing corresponding UTC time values
    utc_datetime = datetime.datetime.utcfromtimestamp(currently_json['time'])
    local_datetime = utc_datetime.replace(tzinfo=datetime.timezone.utc).astimezone(tz=None)
    second = local_datetime.second
    minute = local_datetime.minute
    hour = local_datetime.hour
    day = local_datetime.day
    month = local_datetime.month
    year = local_datetime.year

    context = {
        'lat': location[0],
        'lng': location[1],
        'street': street,
        'city': city,
        'state': state,
        'second': second,
        'minute': minute,
        'hour': hour,
        'day': day,
        'month': month,
        'year': year
    }

    for key in current_weather_info:
        context[key] = current_weather_info[key]

    return render(request, 'app_skycast/current.html', context)


def forecast_view(request):
    global error
    context = {}

    # retrieve form input values for the address
    street = request.POST['input1']
    city = request.POST['input2']
    state = request.POST['input3']

    # retrieve lat/lng with Google geocoding API
    location = get_lat_lng(street, city, state)

    # if lat/lng lookup didn't work, set global 'error' var to true and call index view
    if location[0] == None:
        error=True
        return index_view(request)

    context['lat'] = location[0]
    context['lng'] = location[1]

    query_number = request.session['number_of_queries']

    if query_exists(street, city, query_number, request.session):
        pass
    else:
        query_info = {'street': street, 'city': city, 'state': state}
        # starts at request.session['query0'], then 'query1', 'query2', ... etc.
        request.session['query'+str(query_number)] = query_info
        # incrementing number_of_queries
        request.session['number_of_queries'] += 1

    context['street'] = street
    context['city'] = city
    context['state'] = state

    # use the lat/lng coordinates to query Dark Sky
    forecast_json = get_forecast(location[0], location[1], request.session)

    # picking out the 'hourly' and 'daily' dicts
    hourly_json = forecast_json['hourly']
    daily_json = forecast_json['daily']

    # is there an alert?
    if 'alerts' in forecast_json:
        alerts_json = forecast_json['alerts']
        alerts_data = get_alerts_data(alerts_desired_keys, alerts_json)
        context['alerts_data'] = alerts_data

    # parse forecast response, store data of interest in daily and hourly lists of dicts
    daily_data = get_daily_data(daily_desired_keys, daily_json)
    hourly_data = get_hourly_data(hourly_desired_keys, hourly_json)


    # passing the entire list of dicts to the template
    context['daily_data'] = daily_data
    context['hourly_data'] = hourly_data

    return render(request, 'app_skycast/forecast.html', context)


def historical_index_view(request):
    context = {}
    context['remaining_api_requests'] = '1000'
    context['remaining_api_requests'] = request.session['remaining_api_requests']

    # retrieve form input values for the address inserted in index, to pre-populate the inputs in historical_index
    street = request.POST['input1']
    city = request.POST['input2']
    state = request.POST['input3']

    context['street'] = street
    context['city'] = city
    context['state'] = state

    return render(request, 'app_skycast/historical_index.html', context)


def historical_view(request):
    context={}

    # retrieve form input values for the address
    street = request.POST['input1']
    city = request.POST['input2']
    state = request.POST['input3']
    month = request.POST['month']
    year = request.POST['year']

    location = get_lat_lng(street, city, state)
    one_month_of_data = get_one_month_of_data(month, year, location[0], location[1], request.session)
    context['lat'] = location[0]
    context['lng'] = location[1]
    context['street'] = street
    context['city'] = city
    context['state'] = state
    context['month'] = month
    context['year'] = year

    # passing number of days so template knows max iteration value
    context['number_of_days'] = len(one_month_of_data)

    context['days_data'] = one_month_of_data

    return render(request, 'app_skycast/historical.html', context)

def get_forecast(lat, lng, session):
    key = api_keys.dark_sky_key
    req = requests.get('https://api.darksky.net/forecast/{}/{},{}'
                       .format(key, lat, lng))
    # setting cookie with remaining API queries
    session['remaining_api_requests'] = 1000 - int(req.headers['X-Forecast-API-Calls'])
    req_json = req.json()
    return req_json


def get_one_month_of_data(month, year, lat, lng, session):
    # X-Forecast-API-Calls as a response header

    # Number of days in a month varies... using calendar.monthrange to determine the correct # of days
    # reference as: days_data[day]['daily']['data'][0][key]
    # where day is an int in range (0, num_days_in_month), and key is one of the desired_historical_keys
    num_days_in_month = calendar.monthrange(int(year), int(month))[1]

    # list of length [days_in_month] to hold dicts containing entire 'daily' response from each query
    full_daily_data = [None] * num_days_in_month
    # list of dicts to hold only data of interest from full response
    days_data = [None] * num_days_in_month

    # iterate through every day in the month, 0-indexed
    for i in range(0, num_days_in_month):
        # forming query string of format [YYYY]-[MM]-[DD]T[HH]:[MM]:[SS]
        date_string = "{}-{:0>2}-{:0>2}T00:00:00".format(year, month, str(i + 1))
        # excluding hourly and currently data; only interested in daily
        query_text = "https://api.darksky.net/forecast/{}/{},{},{}?exclude=hourly,currently"\
            .format(api_keys.dark_sky_key, lat, lng, date_string)
        req = requests.get(query_text)
        full_daily_data[i] = req.json()
        days_data[i] = {}
        for key in desired_historical_keys:
            days_data[i][key] = full_daily_data[i]['daily']['data'][0][key]
            print()
        # don't want to set the cookie over and over, just on the final query.
        if i==(num_days_in_month-1):
            # setting cookie with remaining API queries
            session['remaining_api_requests'] = 1000 - int(req.headers['X-Forecast-API-Calls'])

    return days_data


def get_daily_data(daily_desired_keys, daily_json):
    a = [None] * 7
    for i in range(0, 7):
        # using first index as 1 intentionally - index 0 seems to be the previous day instead of current
        # daily times will always be midnight... e.g. 'September 21st 12:00:00AM'
        utc_datetime = datetime.datetime.utcfromtimestamp(daily_json['data'][i + 1]['time'])
        local_datetime = utc_datetime.replace(tzinfo=datetime.timezone.utc).astimezone(tz=None)
        # initialize dict in list
        a[i] = {}
        # setting month and day
        a[i]['month'] = local_datetime.month
        a[i]['day'] = local_datetime.day
        #
        for key in daily_desired_keys:
            # 'data' is a list of dicts
            if key in daily_json['data'][i + 1]:
                # special cases to convert sunsetTime and sunriseTime to meaningful units,
                # only passing hour and minute to context
                if key == 'sunsetTime':
                    sunset_time = datetime.datetime.utcfromtimestamp(daily_json['data'][i+1]['sunsetTime'])
                    local_sunset_time = sunset_time.replace(tzinfo=datetime.timezone.utc).astimezone(tz=None)
                    a[i]['sunsetTime'] = "{}:{:02}".format(local_sunset_time.hour, local_sunset_time.minute)
                elif key == 'sunriseTime':
                    sunrise_time = datetime.datetime.utcfromtimestamp(daily_json['data'][i + 1]['sunriseTime'])
                    local_sunrise_time = sunrise_time.replace(tzinfo=datetime.timezone.utc).astimezone(tz=None)
                    a[i]['sunriseTime'] = "{}:{:02}".format(local_sunrise_time.hour, local_sunrise_time.minute)
                else:
                    a[i][key] = daily_json['data'][i + 1][key]
    return a


def get_hourly_data(hourly_desired_keys, hourly_json):
    a = [None] * 48
    for i in range(0, 48):
        utc_datetime = datetime.datetime.utcfromtimestamp(hourly_json['data'][i]['time'])
        local_datetime = utc_datetime.replace(tzinfo=datetime.timezone.utc).astimezone(tz=None)

        a[i] = {}
        a[i]['month'] = local_datetime.month
        a[i]['hour'] = local_datetime.hour
        a[i]['day'] = local_datetime.day
        a[i]['full_time'] = str(local_datetime.month) + "/" + str(local_datetime.day) + " - " + \
                            str(local_datetime.hour) + ":00"

        for key in hourly_desired_keys:
            if key in hourly_json['data'][i]:
                a[i][key] = hourly_json['data'][i][key]
    return a


def get_alerts_data(alerts_desired_keys, alerts_json):
    a = [None] * len(alerts_json)
    for i in range(0, len(alerts_json)):
        a[i] = {}
        for key in alerts_desired_keys:
            a[i][key] = alerts_json[i][key]
    return a


def get_lat_lng(street_from_form, city_from_form, state_from_form):
    """
    Helper function for the view: weather_from_address_view.
    Uses the form inputs provided by the view function to query the Google Maps API,
    and then parses the JSON response to retrieve and return the latitude and longitude values.
    :param street_from_form: (string) The street name and number.
    :param city_from_form: (string) The city which the street is in.
    :param state_from_form: (string) The state which the city is in.
    :return: (lat, lng) tuple
    """
    # replacing spaces with '+' for proper URL formatting
    street_name = street_from_form.replace(" ", "+")
    city_name = city_from_form.replace(" ", "+")
    state_name = state_from_form.replace(" ", "+")

    # using the requests library to send the HTTP request
    # NOTE: 'verify' parameter is set to False; SSL certificate is not verified. This security risk is not
    # acceptable for a fully-fledged, production application, but seems suitable for simplicity here.
    req = requests.get('https://maps.googleapis.com/maps/api/geocode/'
                       'json?address={},+{},+{}'
                       '&key={}'.format(street_name, city_name, state_name, api_keys.google_key), verify=False)

    req_json = req.json()
    # error checking in case geolocation API fails
    if req_json['status'] == 'ZERO_RESULTS':
        return None, None
    else:
        lat = req_json['results'][0]['geometry']['location']['lat']
        lng = req_json['results'][0]['geometry']['location']['lng']
        # return (lat, lng) tuple
        return lat, lng
