from django.conf.urls import url

from . import views

app_name = 'app_skycast'
urlpatterns = [
    url(r'^$', views.index_view, name='index'),
    url(r'^current', views.current_view, name='current'),
    url(r'^forecast', views.forecast_view, name='forecast'),
    url(r'^historical_index', views.historical_index_view, name='historical_index'),
    url(r'historical', views.historical_view, name='historical')

]