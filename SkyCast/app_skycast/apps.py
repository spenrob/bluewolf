from django.apps import AppConfig


class AppSkycastConfig(AppConfig):
    name = 'app_skycast'
